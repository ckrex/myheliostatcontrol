Source Code for my Homemade Dual Axis Heliostat
A Heliostat is a machine that follows a light source e.g. the sun.

How To COMPILE:
OSX:
You should install homebrew
$ brew tap osx-cross/avr
Then install avr-libc
$ brew install avr-libc

Ater that, go into your directory where your .git dir is and type "make" to compile the Program for the AVR AtMega8.
Edit the Makefile to change the AtMega. To compile for the AtMega 16:
MCU = -mmcu=atmega16